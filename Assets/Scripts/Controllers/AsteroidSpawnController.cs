using UnityEngine;
using System.Collections;

public class AsteroidSpawnController : MonoBehaviour {

  public GameObject[] asteroidModels;
  public float respawnDelay;

  public float minDistance;
  public float maxDistance;

  public int spawnCount;

  private int activeAsteroids = 0;
  private GameObject[] asteroids;
  private bool spawnerActive = false;

	// Use this for initialization
	void Start () {
    asteroids = new GameObject[spawnCount];
    Spawner();
	}

  IEnumerator Spawn(int numberToSpawn) {
    spawnerActive = true;
    for(int i = 0; i < numberToSpawn; i++){

      // pick an asteroid model from our array of models
      int index = Random.Range(0, asteroidModels.Length);

      // Set a random position based on the spawners location
      Vector3 position = transform.position;
      position.x += Random.Range(minDistance, maxDistance) * (i+1) * (i+1);
      position.y = 0f;
      position.z += Random.Range(minDistance, maxDistance) * (i+1) * (i+1);

      asteroids[i] = Instantiate(asteroidModels[index], position, Quaternion.identity) as GameObject;

      // set this asteroids target (the spawner)
      // and set a random rotation
      asteroids[i].GetComponent<OrbitTarget>().SetTarget(this.gameObject);
      asteroids[i].GetComponent<OrbitTarget>().RandomizeRotation();
      asteroids[i].transform.parent = this.transform;

      activeAsteroids++;
      yield return new WaitForSeconds(2f);

    }
    spawnerActive = false;
  }

  public void AsteroidHit() {
    activeAsteroids--;
  }

  void Update() {
    if (!spawnerActive) {
      if (activeAsteroids <= 0) {
        spawnerActive = true;
        Invoke("Spawner", respawnDelay);
      }
    }
  }

  void Spawner() {
    StartCoroutine(Spawn(spawnCount));
  }

  void OnDrawGizmos() {
    Gizmos.color = Color.red;
    Gizmos.DrawWireSphere(transform.position, 1);
  }
}
