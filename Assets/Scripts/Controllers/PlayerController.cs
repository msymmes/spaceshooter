using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {
    public ShipController ship;

    void FixedUpdate () {
        float turn = Input.GetAxis("Horizontal");
        float thrust = Input.GetAxis("Vertical");
        float strafe = Input.GetAxis("Strafe");

        // pass control info to the ship
        ship.Thrust(thrust);
        ship.Turn(turn);
        ship.Strafe(strafe);

    }

    void Update() {
        if (Input.GetButton("Fire1")) {
            ship.Fire();
        }
    }

}
