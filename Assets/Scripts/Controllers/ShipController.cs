using UnityEngine;
using System.Collections;

public class ShipController : MonoBehaviour {

  private Rigidbody rb;

  // Audio Sources
  public AudioClip pickupSound;
  public AudioClip shieldSound;
  public AudioClip weaponSound;

  // game objects
  public GameObject shield;
  public GameObject shot;

  // ship hardpoints for modules
  public Transform shotSpawn;

  // weapons
  private float nextFire = 0.0f;
  public float fireRate = 0.5f;

  // ship stats
  public float turnSpeed = 100f;
  public int speed = 5;
  public float tilt = 3f;
  public float strafePower = 10f;

  // state variables
  private bool _shieldsUp;
  private bool _turning = false;
  private bool _facingUp = false;
  private float _turn;
  private float _strafe;
  private float _thrust;

  private AudioSource engineSound;

  void Start () {
    engineSound = GetComponent<AudioSource>();
    rb = GetComponent<Rigidbody>();
  }

  public void Thrust(float t){
    // set thrust state variable
    _thrust = t;

    // adjust engine sound based on the amount of thrust
    engineSound.volume = Mathf.Clamp(Mathf.Abs(_thrust), 0.1f, .5f);

    // add force
    rb.AddForce(transform.forward * _thrust * speed);

  }

  public void Turn(float t){
    _turn = t;
    rb.AddTorque(0.0f, t * turnSpeed, 0.0f);
  }

  public void Strafe(float strafe){
    _strafe = strafe;

    // set up common vectors
    Vector3 forward = transform.forward;
    Vector3 up = new Vector3(0.0f, 1.0f, 0.0f);

    // find current left and right vectors:
    Vector3 right = Vector3.Cross(forward.normalized, up.normalized);
    Vector3 left = -right;

    // add strafe forces
    if (strafe > 0){
      rb.AddForce(left * strafePower);
    } else if (strafe < 0){
      rb.AddForce(right * strafePower);
    }

  }

  public void Fire(){
    if (Time.time > nextFire){
      nextFire = Time.time + fireRate;

      GameObject spawnedObject = Instantiate(shot, shotSpawn.position, Quaternion.identity) as GameObject;

      Projectile projectile = spawnedObject.GetComponent<Projectile>();
      projectile.SetDirection(transform.forward, rb.velocity);

      AudioSource.PlayClipAtPoint(weaponSound, transform.position);
    }
  }

  void FixedUpdate(){
    float rotationZ = 0.0f;

    // strafe handling
    if (_strafe != 0 || _turn != 0){

      // Keep track of turn/strafe state
      if (!_turning){
        if(transform.forward.z > 0){
            _facingUp = true;
        } else if (transform.forward.z < 0){
            _facingUp = false;
        }
        _turning = true;
      }

      // calculate z rotation based on turn/strafe
      if(_strafe != 0){
        if (_facingUp){
          rotationZ = _strafe * -tilt * 5;
        } else {
          rotationZ = -(_strafe * tilt * 5);
        }
      }
      if (_turn != 0){
        rotationZ = (_turn * tilt * 10);
      }

      // apply the tilt rotation
      rb.rotation = Quaternion.Euler(
            transform.eulerAngles.x,
            transform.eulerAngles.y,
            Mathf.Clamp(rotationZ, -45, 45)
      );

    } else {

      // reset turn state
      _turning = false;

      // slerp our tilt back to 0
      rb.rotation = Quaternion.Slerp(
            transform.rotation,
            Quaternion.Euler(
                transform.eulerAngles.x,
                transform.eulerAngles.y,
                0.0f
            ),
            Time.deltaTime * transform.eulerAngles.z
      );
    }
    transform.position = new Vector3(transform.position.x, 0.0f, transform.position.z);
  }

  public void PowerUp(int type){

    // play pickup sound
    AudioSource.PlayClipAtPoint(pickupSound, transform.position);

    if (type == 0){
      fireRate -= 0.025f;
    } else if (type == 1){
      // spawn a shield (or recharge existing)
      ShieldsUp();
    } else if (type == 2){
      speed += 3;
    } else {
      Debug.Log("Incorrect Values for PowerUP Type given. Should be an int. Instead was: " + type);
    }
  }

  void ShieldsUp(){
    if(_shieldsUp){
      shield.GetComponent<ShieldController>().Boost(3);
    } else {
      GameObject s = Instantiate(shield, transform.position, transform.rotation) as GameObject;
      s.transform.parent = transform;
      _shieldsUp = true;
      PlayShieldPowerSound();
    }
  }

  public void ShieldsDown(){
    _shieldsUp = false;
    PlayShieldPowerSound();
  }

  void PlayShieldPowerSound(){
    AudioSource.PlayClipAtPoint(shieldSound, transform.position);
  }
}
