using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

    [SerializeField] private GameObject _target;

    public Vector3 offset;
    public float speed = 0f;
    public float bounceDistance = 5f;

	// Update is called once per frame
	void FixedUpdate () {

        if(_target){
            float distance = Vector3.Distance(_target.transform.position, transform.position);
            distance = distance / bounceDistance;

            Vector3 pos = new Vector3(
                    _target.transform.position.x + offset.x,
                    _target.transform.position.y + offset.y,
                    _target.transform.position.z + offset.z
            );

            transform.position = Vector3.Lerp(transform.position, pos, Time.deltaTime * (speed + distance));

        }

	}
}
