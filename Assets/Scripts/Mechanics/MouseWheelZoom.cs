using UnityEngine;
using System.Collections;

public class MouseWheelZoom : MonoBehaviour {

    public float minZoom = 10;
    public float maxZoom = 50;

	void Update () {

        if(Input.GetAxis("Mouse ScrollWheel") < 0){
            Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize - 1, minZoom, maxZoom);
        } else if (Input.GetAxis("Mouse ScrollWheel") > 0){
            Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize + 1, minZoom, maxZoom);
        }
	}
}
