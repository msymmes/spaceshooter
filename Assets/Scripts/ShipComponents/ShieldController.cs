using UnityEngine;
using System.Collections;

public class ShieldController : MonoBehaviour {

    private Transform playerShip;
    public GameObject explosion;
    private GameController gameController;
    //private Rigidbody rb;

    public float hitPoints;

    void Start(){
        //rb = GetComponent<Rigidbody>();
        playerShip = GameObject.FindWithTag("Player").transform.GetChild(0);

        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if(gameControllerObject != null){
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if(gameController == null){
          Debug.Log("Cannot find 'GameController' script");
        }
    }

    void Update(){
      transform.position = new Vector3(playerShip.position.x, playerShip.position.y, playerShip.position.z - 0.01f);
    }

    void OnTriggerEnter(Collider other){

        if (other.CompareTag("Shield") ||
            other.CompareTag("Ship") ||
            other.CompareTag("Boundary") ||
            other.CompareTag("PowerUP"))
        {
            return;
        }

        hitPoints -= 1;

        if (hitPoints <= 0){

          Destroy(gameObject);
          playerShip.GetComponent<ShipController>().ShieldsDown();

          if(explosion != null){
            Instantiate(explosion, transform.position, transform.rotation);
          }
        }

        Attackable attackable = other.gameObject.GetComponent<Attackable>();
        if (attackable) {
          attackable.Attack("basic", 5);
        }

    }

    public void Boost(int amount){
      hitPoints += amount;
    }
}
