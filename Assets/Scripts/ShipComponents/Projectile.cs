using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

  private Rigidbody rb;
  public float speed;
  private Vector3 _velocity;
  private Vector3 _direction;


  public void SetDirection(Vector3 direction, Vector3 v){
    _direction = direction;
    _velocity = v;
    transform.forward = _direction;
  }

	// Use this for initialization
	void Start () {
    rb = GetComponent<Rigidbody>();
    SetVelocity();
	}

  void Update(){
    SetVelocity();
    // keep the object attached to the 0 coord on the y axis
    transform.position = new Vector3(transform.position.x, 0.0f, transform.position.z);
  }

  void SetVelocity() {
    if (rb) {
      //rb.AddForce((_direction * speed) + _velocity * 4);
      rb.velocity = _direction * speed + _velocity;
    }
  }

  void OnTriggerEnter (Collider other) {
    Debug.Log("hit " + other.gameObject.name);
    Destroy(other.gameObject);
  }


}
