using UnityEngine;
using System.Collections;

public class ConsumePowerUP : MonoBehaviour {

    private GameController gameController;
    private PlayerController playerController;
    private ShipController ship;


    void Start(){
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");

        if(gameControllerObject != null){
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if(gameController == null){
          Debug.Log("Cannot find 'GameController' script");
        }

        GameObject playerControllerObject = GameObject.FindWithTag("Player");
        if(playerControllerObject != null){
            playerController = playerControllerObject.GetComponent<PlayerController>();
        }
        if(playerController == null){
          Debug.Log("Cannot find 'PlayerController' script");
        }

        ship = playerController.ship;

    }

    void OnTriggerEnter(Collider other){

        if (other.CompareTag("Boundary") || other.CompareTag("Enemy"))
        {
            return;
        }

        if(other.tag == "Ship" || other.tag == "Shield"){
          switch(gameObject.name){
            case "Powerup-WeaponSpeed":
            case "Powerup-WeaponSpeed(Clone)":
              ship.PowerUp(0);
              break;
            case "Powerup-Shield":
            case "Powerup-Shield(Clone)":
              ship.PowerUp(1);
              break;
            case "Powerup-Speed":
            case "Powerup-Speed(Clone)":
              ship.PowerUp(2);
              break;
            default:
              Debug.Log("Invalid Powerup type: " + gameObject.name);
              break;
          }
        }

        Destroy(gameObject);
    }
}
