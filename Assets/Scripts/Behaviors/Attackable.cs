using UnityEngine;
using System.Collections;

public class Attackable : MonoBehaviour {

  public GameObject explosion;

  public void Attack(string type, int value) {
    Debug.Log(gameObject.name + " attacked with " + type + " for " + value);
    Destroy(gameObject);
    Instantiate(explosion, transform.position, transform.rotation);
  }
}
