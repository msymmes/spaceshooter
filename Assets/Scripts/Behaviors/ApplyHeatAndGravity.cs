using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ApplyHeatAndGravity : MonoBehaviour {

  [SerializeField] private Text warningText;

  public float radius;
  public float gravity;

	void Update () {
    Collider[] hitColliders = Physics.OverlapSphere(transform.position, radius);
    int i = 0;
    while (i < hitColliders.Length) {
      GameObject thisObject = hitColliders[i].gameObject;
      if (thisObject.CompareTag("Ship") || thisObject.CompareTag("Shield")) {
        // attempt the get the objects rigidbody
        Rigidbody objectInSpace = thisObject.GetComponent<Rigidbody>();
        if (objectInSpace) {

          // calculate (distance and direction
          float distance = Vector3.Distance(transform.position, thisObject.transform.position);
          Vector3 direction = (transform.position - thisObject.transform.position);
          /*
          // add a force to the objects rigidbody that
          // pulls the object more the closer it gets
          Vector3 force = (direction * gravity) / (distance*2);
          Debug.Log(force);
          objectInSpace.AddForce(force);
          */
          Vector3 force = ((direction * gravity) / (distance) / distance);
          objectInSpace.velocity += force;

        }
      }
      i++;
    }
  }

  // On Collision Give warning.
  void OnTriggerEnter(Collider other) {
    print ("You have entered the danger zone: " + other.gameObject.name);
    if (other.CompareTag("Ship") || other.CompareTag("Shield")) {
      warningText.GetComponent<Animator>().SetBool("isActive", true);
      warningText.GetComponent<AudioSource>().Play();
    }
  }

  void OnTriggerExit(Collider other) {
    if (other.CompareTag("Ship") || other.CompareTag("Shield")) {
      warningText.GetComponent<Animator>().SetBool("isActive", false);
      warningText.GetComponent<AudioSource>().Stop();
    }
  }
}
