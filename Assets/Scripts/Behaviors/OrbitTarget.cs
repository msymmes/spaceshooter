using UnityEngine;
using System.Collections;

 public class OrbitTarget : MonoBehaviour {

   [SerializeField] private GameObject target;
   [SerializeField] private bool maintainYOfZero;

   private Transform center;
   private Vector3 axis = Vector3.up;
   private Vector3 desiredPosition;

   public float radius = 0f;
   public float radiusSpeed = 0.5f;
   public float rotationSpeed = 10.0f;

   void Start () {

     if (radius == 0f) {
       radius = Vector3.Distance(transform.position, center.position);
     }

     if (target){
       Setup();
     }
   }

   void Setup() {
     if (target) {
       center = target.transform;
       float startY = transform.position.y;
       Vector3 position = (transform.position - center.position).normalized * radius + center.position;
       position.y = startY;
       transform.position = position;

     } else {
       Debug.Log("No target while trying to set center");
     }
   }

   public void SetTarget(GameObject t){
     target = t;
     Setup();
   }

   public void RandomizeRotation(){
     radiusSpeed = 10f;
     rotationSpeed = Random.Range(1f, 20f);
   }

   void Update () {
     if (target && center){
       transform.RotateAround (center.position, axis, rotationSpeed * Time.deltaTime);
       desiredPosition = (transform.position - center.position).normalized * radius + center.position;

       transform.position = Vector3.MoveTowards(transform.position, desiredPosition, Time.deltaTime * radiusSpeed);
       if (maintainYOfZero) {
         transform.position = new Vector3(
               transform.position.x,
               0.0f,
               transform.position.z
         );

       }
     } else {
       Debug.Log(gameObject.name + " not updating position");
       Debug.Log("Target is : " + target);
       Debug.Log("Center is : " + center);
     }
   }
 }
