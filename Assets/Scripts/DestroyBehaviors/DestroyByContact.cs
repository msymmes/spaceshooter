using UnityEngine;
using System.Collections;

public class DestroyByContact : MonoBehaviour
{
    public int scoreValue;

    public GameObject[] powerUPs;
    public GameObject explosion;
    public GameObject playerExplosion;

    private GameController gameController;

    void Start(){
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if(gameControllerObject != null){
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if(gameController == null){
          Debug.Log("Cannot find 'GameController' script");
        }
    }

    void OnTriggerEnter(Collider other){

        if (other.CompareTag("PowerUP") || other.CompareTag("Boundary") || other.CompareTag("Enemy") || other.CompareTag("Shield"))
        {
            return;
        }

        if(explosion != null){
          Instantiate(explosion, transform.position, transform.rotation);
          transform.parent.GetComponent<AsteroidSpawnController>().AsteroidHit();
        }

        if(other.tag == "Player"){
            Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
            // Handle Player death here

        } else {

          if (Random.Range(1, 10) > 5){
              GameObject powerUP = powerUPs[Random.Range(0, powerUPs.Length)];
              Vector3 pos = new Vector3(transform.position.x, 0.0f, transform.position.z);
              Instantiate(powerUP, pos, Quaternion.identity);
          }
        }

        Destroy(other.gameObject);
        Destroy(gameObject);
    }
}
