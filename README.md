# Spaceshooter

> This is my attempt to learn Unity3d by extending the official Unity3d Space Shooter tutorial.
> The goal is a single player game that has a more "open"/flexible game play style, while maintaining the arcade feel.
> The player will be able to upgrade ships and weapons, interact with objects in space, facilitate trading across planets, and space stations as well as build thier own pieces of the industrial chain through both planetary and space-station-based mining, research, and production facilities.


###### What works
- The world is much larger, though it uses the same background (For now).
- Turning:
  - the ship can now face/move in all directions (along the same 2d plane the original game uses).
- asteroids/enemies drop powerups.
- shields!
- Solar systems... with suns, planets and moons....

##### In Progress
- The player can use different ships (partially complete)
- Player can switch out/upgrade weapons

###### TODO
- Mouselook
- Targeting
- Turrets
- Modules (components that can be added/removed from a ship)
